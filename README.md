# netdata on docker-host

<p align="center"><a href="https://netdata.cloud"><img src="https://user-images.githubusercontent.com/1153921/95268672-a3665100-07ec-11eb-8078-db619486d6ad.png" alt="Netdata" width="300" /></a></p>

<h3 align="center">Netdata is high-fidelity infrastructure monitoring and troubleshooting.<br />Open-source, free, preconfigured, opinionated, and always real-time.</h3>
<br />

<img src="https://user-images.githubusercontent.com/1153921/95269366-1b814680-07ee-11eb-8ff4-c1b0b8758499.png" alt="---" style="max-width: 100%;" /><br /><br />

GitHub : https://github.com/netdata/netdata  
Documentation: https://docs.netdata.cloud  

## Environment file

```bash
#!/usr/bin/env
# Enviroment variables for Compose
# Source: https://docs.docker.com/compose/environment-variables/#the-env-file
#
TZ=Europe/Zurich
NODE_HOSTNAME=netdata.youname.it
```


## Secure way for Docker Container Names Resolution with **Tecnativa's Docker Socket Proxy**

Grant netdate socket permissions is unsave if the container goes public
grant a socket proxy permissions is better

Netdata Docs: https://docs.netdata.cloud/packaging/docker/#docker-container-names-resolution

GitHub : https://github.com/Tecnativa/docker-socket-proxy

```yaml
...
  netdata
    ...
    environment:
      - TZ
      - DOCKER_HOST=dsproxy:2375
  dsproxy:
    image: tecnativa/docker-socket-proxy:latest
    container_name: docker-socket-proxy
    restart: unless-stopped
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    expose: 
      - 2375
    privileged: true
    environment:
      - CONTAINERS=1
```
